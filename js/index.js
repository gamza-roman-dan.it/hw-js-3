"use strict"

//1-
//Логічні оператори використовуються для визначення логіки між змінними або значеннями.
//2-
//У JavaScript є три логічні оператори: && (І), || (АБО), ! (НЕ)

//1-
const userYears = prompt("вкажіть свій вік будьласка");
if (isNaN(userYears)){
    alert("вкажіть вік цифрою");
}else if (userYears <= 12){
    alert("ви є ще дитиною");
}else if (userYears <= 18){
    alert("ви є ще підлітком");
}else{
    alert("ви є уже дорослим");
}

//2-
const month = prompt("вкажіть місяць будьласка");
switch (month){
    case"жовтень":
    case"серпень":
    case"липень":
    case"травень":
    case"березень":
    case"січень":
    case"грудень":{
        console.log("31 день");
        break;
    }
    case"лютий":{
        console.log("29 день");
        break;
    }
    case"листопад":
    case"вересень":
    case"червень":
    case"квітень":{
        console.log("30 день");
        break;
    }
    default:{
        console.log(0);
    }
}